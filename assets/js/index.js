
const sampleFunction = () => {
	alert("Hi");
};
//sampleFunction();


// implicit return with arrow function 

function prodNum(num1,num2){
	return num1 * num2;
}

let product = prodNum(2,5);
console.log(product);

// arrow function with implicit return 
// one liner lang and walang curly braces 
const addNum = (num1, num2) => num1 + num2;
let add = addNum(2,5);
console.log(add);

//arrow function with implicit return
// when using curly braces you need a return. 
const divNum = (num1, num2) => {
	return num1 / num2;
}

let divide = divNum(6,2);
console.log(divide);

//ternary ? - symbolizes and if 
//        : - symbolize else  
const findGender = (gender) => (gender = "Male")? 'true' : 'false';
let gender = findGender("Male");
console.log(gender);


// arrow function with if else, braces and return.
let gradeValue;
const generateGrade = (grade) => {
	if(grade > 90){
		gradeValue = "A";
	}else{
		gradeValue = "F";
	}
	return gradeValue;
}

let a = generateGrade(91);
console.log(a);
let b = generateGrade(88);
console.log(b);





/*
	Mini Activity 
*/
let numbers = [1, 2, 3, 4, 5];

let allValid = numbers.every((number) =>{
	return (number < 3)
});

let filterValid = numbers.filter((number) =>{
	return (number < 3)
})

let numberMap = numbers.map((number) => {
	return number * number
})

console.log(allValid);
console.log(filterValid);
console.log(numberMap);

// to check if number is positive, negative or zero
let num = 0;
let result = (num >= 0) ? (num == 0 ? 'zero' : 'positive') : 'negative';
console.log(`The number is ${result}`);



/*
		--------------------------	Start of JSON --------------------------------------
		JSON - javascript object notation. 
		     - string but formatted as JS Object. 
		     - is a popular used to pass data from one application to another. 
		     - is not only for javascript but also for other programming language. 
		     - this is why it is specified as a js notation.
		     - file ext. `filename.json` 
		     - there is a way to turn json as js object and vice versa. 
		     - the language of data transfer.
		     - json supports `string`,`number`,`object`,`array`,`boolean`,`null`

		!important: 
		* js object are not the same as json because json is a string and js object is an object. 
		* json keys are surrounded by double qoutes "".
	
		* Parsing Dates - Date objects are not allowed in JSON. 
			- if you need to include a date, write it as a string you can convert it back into a date object later.
		
		- JSON Format is use for database. 
		
		Syntax JSON : 
			{
				"key1" : "value1",
				"key2" : "value2"
			}

		!important: 	
		* JSON.stringify - method to convert JS Object to JSON. 
		* JSON.parse();
			JSON.parse - method to convert JSON to JS.
			- converts data to a javascript object.
			using parse  '{...}' add a quote to change to string.
*/

const person = {
	"name" : "Juan",
	"weight" : 175,
	"age" : 20,
	"eyeColor" : "brown",
	"cars" : ["toyota","honda"],
	"favoriteBooks" : {
		title : "Avatar",
		author: "Nickelodeon",
		releaseDate : 2021
	}
};
console.log(person);

const assets = [
	{
		"id" : 1,
		"name" : "James",
		"decription" : " ",
		"isAvailable" : true,
		"dateAdded" : "2021-10-13"
	},
	{
	"id" : 2,
	"name" : "Mark",
	"decription" : " ",
	"isAvailable" : false,
	"dateAdded" : "2021-10-13"
	}
]

const getAssets = () =>{
	for(let index = 0; index < assets.length; index++){
		console.log(assets[index]);
	}
} 
getAssets();



//JSON.stringify - method to convert JS Object to JSON. 
let cat = {
	"name" : "Mashiro",
	"age" : 3,
	"weight" : 20
}

console.log(cat);
const catJSON = JSON.stringify(cat);
console.log(catJSON);


//JSON.parse - method to convert JSON to JS. 
const catParse = JSON.parse(catJSON);
console.log(catParse);

// JSON.parse();
// syntax : const or let  variable_name = JSON.parse(json_variableName);
const cars = '["toyota","honda"]'
const car1 = JSON.parse(cars);
console.log(car1[0]);


 const json = '{"name":"Mashiro","age":3,"weight":20}';
 const cat1 = JSON.parse(json);
 console.log(cat1);

 let batchesArr = [
 	{
 		batchName : "Batch 131"
 	},
 	{
 		batchName : "Batch 132"
 	}
 ];
 console.log(JSON.stringify(batchesArr));

 let batchesJSON = `[
 	{
 		"batchName":"Batch 131"
 	},
 	{
 		"batchName":"Batch 132"
 	}
]`;

console.log(JSON.parse(batchesJSON));




/* CRUD function when inserting an object to an array */
let courseArray = [];

let coursename = document.querySelector('#courseName').value;
let coursedesc = document.querySelector('#courseDesc').value;
let courseprice = document.querySelector('#coursePrice').value;


/*const addCourse = (name,desc,price) =>{
	validation();
}
const validation =() => {
	if(coursename == null, )
}
*/

//add course
const postCourse = (id, name, description, price, isActive) =>{
	let course = new Object();
	course.id  = id;
	course.name = name;
	course.description = description;
	course.price = price;
	course.isActive = isActive	

	//add to array  
	if(course != " "){
		courseArray.push(course);
		alert(`Successfully Added Course ${course.name}!`);
	}else{
		alert(`Failed to Add Course ${course.name}!`);
	}
}

postCourse(1,'JS','Javascript',155, true);
postCourse(2,'CSS','Cascading',55, true);

//find Course
const findCourse = (id) =>{
	for(let index = 0; index < courseArray.length; index++){
		let temp = courseArray[index];
		if(temp.id == id){
			alert(`Course ${temp.name} available!`);
			console.log(temp);
		}
	}
}
findCourse(2);

//delete course 
const removeCourse = (id) =>{
	for(let index = 0; index < courseArray.length; index++){
		let temp = courseArray[index];
		if(temp.id == id){
			courseArray.pop(index);
			alert(`Successfully to Deleted Course ${temp.name}!`);
		}else{
			alert(`Failed to Deleted Course ${temp.name}!`);
		}
	}
}
removeCourse(1);

















/*$('button').bind('click', getUserName = () => {
	let username = $('userName').value;
	console.log(username);
});

*/